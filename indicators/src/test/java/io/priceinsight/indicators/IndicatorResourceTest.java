package io.priceinsight.indicators;

import io.priceinsight.indicators.entities.Indicator;
import io.priceinsight.indicators.entities.IndicatorInput;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;



@QuarkusTest
public class IndicatorResourceTest {
	
	Indicator indicator = new Indicator();
	
	String jsonpayload = "{\n" + 
			"  \"inputs\": [\n" + 
			"    {\n" + 
			"      \"datatype\": \"Double\",\n" + 
			"      \"description\": \"string\",\n" + 
			"      \"name\": \"string\"\n" + 
			"    }\n" + 
			"  ],\n" + 
			"  \"name\": \"bb\"\n" + 
			"}";
	
	String expected = "{\n" + 
			"  \"id\": 1,\n" + 
			"  \"inputs\": [\n" + 
			"    {\n" + 
			"      \"datatype\": \"Double\",\n" + 
			"      \"description\": \"string\",\n" + 
			"      \"id\": 1,\n" + 
			"      \"name\": \"string\"\n" + 
			"    }\n" + 
			"  ],\n" + 
			"  \"name\": \"bb\"\n" + 
			"}";
	
	
	// java.lang.NoSuchMethodError: 'java.lang.String io.priceinsight.indicators.entities.AbstractIndicatorValue.$$_hibernate_read_name()'
//    @Test
//    public void testIndicatorPostEndpoint() {
//    	
//    	indicator.name = "my indicator";
//    	
//    	IndicatorInput i1 = new IndicatorInput();
//    	i1.description = "description 1";
//    	i1.description = "name 1";
//    	
//    	indicator.inputs = new ArrayList<IndicatorInput>();
//    	indicator.inputs.add(i1);
//    	
//        given()
//        .body(indicator)
//          .when().post("/indicators")
//          .then()
//             .statusCode(200)
//             .body(is("hello"));
//    }
    
    @Test
    public void testIndicatorJsonstring() {    	
    	Response res = given()
    	.contentType(ContentType.JSON)
        .body(jsonpayload)
          .when().post("/indicators")
          .then()
             .statusCode(201)
             .extract().response();
//             .path("name");
    	String name = res.as(Indicator.class).name;
    	System.out.println(name);
//             .body(is(exp));
    }
    
    @Test
    public void testMissingName() {    	
    	
    	String jsonpayload = "{\n" + 
    			"  \"inputs\": [\n" + 
    			"    {\n" + 
    			"      \"datatype\": \"Double\",\n" + 
    			"      \"description\": \"string\",\n" + 
    			"      \"name\": \"string\"\n" + 
    			"    }\n" + 
    			"  ],\n" + 
    			"  \"name\": \"\"\n" + 
    			"}";
    	
    	
    	Response res = given()
    	.contentType(ContentType.JSON)
        .body(jsonpayload)
          .when().post("/indicators")
          .then()
             .statusCode(400)
             .extract().response();
//             .path("name");
    	//String name = res.getHeaders().toString();
    	String name = res.as(Object.class).toString();
    	System.out.println(name);
//             .body(is(exp));
    }

}