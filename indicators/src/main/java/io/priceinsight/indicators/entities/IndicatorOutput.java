package io.priceinsight.indicators.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import io.quarkus.runtime.annotations.RegisterForReflection;
@RegisterForReflection
@Entity
public class IndicatorOutput extends AbstractIndicatorValue  {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
	
	public String color;
	public String style;

}
