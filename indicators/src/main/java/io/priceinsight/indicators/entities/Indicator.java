package io.priceinsight.indicators.entities;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.runtime.annotations.RegisterForReflection;

@Entity
@RegisterForReflection
public class Indicator extends PanacheEntity {
	
	
	@NotBlank(message="Name may not be blank")
	public String name;
	
	
	
	@OneToMany(mappedBy="id", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	public Set<IndicatorOutput> outputs;
	
	@OneToMany(mappedBy="id", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	public Set<IndicatorInput> inputs;
	
	public Indicator() {
		super();
	}
}
