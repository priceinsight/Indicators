package io.priceinsight.indicators.entities;


public enum Datatype {
	Double,
	Int,
	Percentage

}
