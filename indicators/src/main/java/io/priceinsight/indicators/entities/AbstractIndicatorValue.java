package io.priceinsight.indicators.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.MappedSuperclass;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
//@Entity
//@Inheritance
@MappedSuperclass
public class AbstractIndicatorValue extends PanacheEntityBase{
	
	public String name;
	public String description;
	public Datatype datatype;
}
