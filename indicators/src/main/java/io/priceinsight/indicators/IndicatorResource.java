package io.priceinsight.indicators;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Variant;

import io.priceinsight.indicators.entities.Indicator;
import io.priceinsight.indicators.entities.exception.ConstraintViolationExceptionMapper;
import io.priceinsight.indicators.repositories.IndicatorRepository;
import jdk.internal.jline.internal.Log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/indicators")
@Produces("application/json")
@Consumes("application/json")
public class IndicatorResource {
	
	private static final Logger LOG = LoggerFactory.getLogger(IndicatorResource.class);

	
	@Inject
	Validator validator;
	
	@Inject
	IndicatorRepository indicatorRepository;

	@POST
    @Transactional
    public Response create(Indicator indicator) {   
		
		//Log.info(indicator.inputs);
		Set<ConstraintViolation<Indicator>> violations = validator.validate(indicator);
	    if (!violations.isEmpty()) {
	    	return new ConstraintViolationExceptionMapper().toResponse(violations);
	    }

		indicator.persist();
        return Response.ok(indicator).status(201).build();
    }
    
    @GET    
    public List<Indicator> indicators() {
    	
        return indicatorRepository.listAll();
    }
}